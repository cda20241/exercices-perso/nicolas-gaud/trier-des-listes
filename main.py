import operator
import random

PEOPLE = [['Pascal', 'ROSE', 43],
          ['Mickaël', 'FLEUR', 29],
          ["Henri", "TULIPE", 35],
          ["BETA", "FRAMBOISE", 35],
          ["Nico", "FRAMBOISE", 35],
          ["Arthur", "PETALE", 65],
          ["Michel", "POLLEN", 50],
          ["Michel", "FRAMBOISE", 42]]

FIRST_NAME_INDEX = 0
NAME_INDEX = 1
AGE_INDEX = 2

random.shuffle(PEOPLE)


def sortpeople(listpeople, index):
    """"
    :param A list of people and an index
    :This function sorts this list by name, first name or age depending on the index
    :return: The list sorted
    """
    for i in range(len(listpeople)):
        mini = listpeople[i]
        j = i - 1
        while j >= 0 and listpeople[j][index] > mini[index]:
            listpeople[j + 1] = listpeople[j]
            j -= 1
        listpeople[j + 1] = mini
    return listpeople


def formatlist(listpeople):
    """"
    :param A list of people
    :This function formats each person in the list and then shows it
    :return: none
    """
    for person in listpeople:
        print(f'{person[FIRST_NAME_INDEX]} {person[NAME_INDEX]} {person[AGE_INDEX]} ans')


print(sortpeople(PEOPLE, NAME_INDEX))
print(sortpeople(PEOPLE, FIRST_NAME_INDEX))
print(sortpeople(PEOPLE, AGE_INDEX))
#Sorting first by name then by first name
print(formatlist(sorted(PEOPLE, key=lambda person: (person[NAME_INDEX], person[FIRST_NAME_INDEX]))))

